package com.geektechniquestudios.profilesexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ProfilesExerciseApplication {

    public static void main(String[] args) {

        ApplicationContext ctx = SpringApplication.run(ProfilesExerciseApplication.class, args);

        System.out.println(ctx.getBean("datasource").toString());
    }


}
